#!/usr/bin/env python

import numpy as np
import os

from ase.calculators.calculator import Calculator
from ase.data import atomic_numbers
from ase.io.trajectory import TrajectoryWriter
from ase import io
from amp.model import LossFunction

class AmpQMMM(Calculator):
    """ The QMMM calculator based on AMP and a DFT calculator. """
    implemented_properties = ['energy', 'forces']

    def __init__(self, selection, qmcalc, mmcalc, qmpbc=None, qmvac=None,
                 qmcell=None, retrain=None, traintraj=[]):
        '''
        parameters:

        selection: list of int, QM region atoms indices
            Selection out of all the atoms that belong to the QM part.
        qmcalc: Calculator object
            QM-calculator.
        mmcalc: Calculator object
            MM-calculator. We use AMP here.
        qmpbc: one or three bool, default is None
            None, the same as the atoms object
            Periodic boundary conditions flags of the QM region.
            Examples: True, False, 0, 1, (1, 1, 0), (True, False, False).
        qmvac: float, default is None
            None, the same unit cell size as the whole Atoms object
            If vacuum=10.0 there will thus be 10 Angstrom of vacuum
            on each side of axis=0 (x axis).
        qmcell: 3x3 matrix or length 3 or 6 vector
            default is None, same as the whole Atoms object unit cell
            QM region unit cell
            If both qmvac and qmcell specified, qmcell overwrites qmvac.
        retrain: dict or None, default is None
            None, the AMP will not be retrained.
            Dict, it passes the convergence criteria to retrain AMP by QM calculations.
            such as {'energy_maxresid': 2.0e-3,
                     'force_maxresid': 5.0e-2}
                     {'energy_rmse': 0.001,
                      'energy_maxresid': None,
                      'force_rmse': 0.005,
                      'force_maxresid': None, }
        traintraj: the list of atoms object, images used to train the mmcalc before
        '''
        self.selection = selection
        self.qmcalc = qmcalc
        self.mmcalc = mmcalc
        self.qmatoms = None
        self.qmpbc = qmpbc
        self.qmvac = qmvac
        self.qmcell = qmcell
        self.retrain = retrain
        self.qmtraj = './qmtraj.traj'
        self.traintraj = traintraj
        try:
            self.name = 'QM region {0} - QM region {1} + All {1}'.format(qmcalc.name, mmcalc.name)
        except: # Dacapo instance has no attribute 'name'
            self.name = 'QM region - QM region {0} + All {0}'.format(mmcalc.name)

        Calculator.__init__(self)

    def initialize_qm(self, atoms):
        constraints = atoms.constraints
        atoms.constraints = []
        self.qmatoms = atoms[self.selection]
        atoms.constraints = constraints
        if self.qmpbc:
            self.qmatoms.pbc = self.qmpbc
        if self.qmvac:
            self.qmatoms.center(vacuum=self.qmvac, axis=0)
        if self.qmcell:
            self.qmatoms.set_cell(self.qmcell, scale_atoms=False)
            self.qmatoms.center()

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        if self.qmatoms is None:
            self.initialize_qm(atoms)

        energy = self.mmcalc.get_potential_energy(atoms)
        forces = self.mmcalc.get_forces(atoms)

        self.qmatoms.set_calculator(self.qmcalc)
        energy += self.qmcalc.get_potential_energy(self.qmatoms)
        qmforces = self.qmcalc.get_forces(self.qmatoms)
        forces[self.selection] += qmforces
        writer = TrajectoryWriter(self.qmtraj, mode='a', atoms=self.qmatoms)
        writer.write()

        energy -= self.mmcalc.get_potential_energy(self.qmatoms)
        mmqmforces = self.mmcalc.get_forces(self.qmatoms)
        forces[self.selection] -= mmqmforces

        self.results['energy'] = energy
        self.results['forces'] = forces

        if self.retrain:
            self.retrain_amp()

    def retrain_amp(self):
        images = self.readimages()
        calc = self.mmcalc
        calc.model.lossfunction = LossFunction(convergence=self.retrain)
        calc.train(images=images) #use the max force res as the criteria
        self.mmcalc = calc

    def readimages(self):
        atoms = io.read(self.qmtraj, index=-1)
        self.traintraj.append(atoms)
        return self.traintraj
