# AmpQMMM #

This is a QM/MM calculator based on a DFT calculator as the quantum mechanics (QM) calculator and AMP as the molecular mechanics (MM) calculator.

## Requirements ##
* Atomic Simulation Environment (ASE) https://wiki.fysik.dtu.dk/ase/
* AMP http://amp.readthedocs.io/en/latest/index.html