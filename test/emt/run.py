#!/usr/bin/env python
import sys
sys.path.append('../../AmpQMMM')

from AmpQMMM import AmpQMMM
from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from ase import Atoms, io
from ase.calculators.emt import EMT
from ase.optimize import BFGS
from ase.visualize import view
from ase.constraints import FixAtoms

atoms = io.read('./Cu_1.traj')
atoms.set_pbc((0,1,0))
indices=[atom.index for atom in atoms if atom.tag==0]
atoms.set_constraint(FixAtoms(indices=indices))
selection = range(27,54)

ampcalc = Amp.load('./emtcalc.amp')
qmcalc = EMT()

calc = AmpQMMM(selection=selection,
               qmcalc=qmcalc,
               mmcalc=ampcalc,
               qmvac=5,
               retain=None)
atoms.set_calculator(calc)
ampqmmm_e = atoms.get_potential_energy()
print 'AmpQMMM Potential Energy'
print ampqmmm_e
qmatoms = calc.qmatoms
view(qmatoms)

atoms.set_calculator(qmcalc)
emt_e = atoms.get_potential_energy()
print 'EMT Potential Energy'
print emt_e
print 'Potential energy difference'
print ampqmmm_e - emt_e

atoms.set_calculator(calc)
dyn = BFGS(atoms, logfile='qn.log', trajectory='qn.traj' )
dyn.run(fmax=0.05)
print 'After geometric optimization: The potential energy is %f' % atoms.get_potential_energy()
