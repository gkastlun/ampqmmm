#!/usr/bin/env python
import sys, os
sys.path.append('../..')

from AmpQMMM_gpaw import AmpQMMM
from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from ase import Atoms, io
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.optimize import BFGS

from gpaw import GPAW, FermiDirac
from gpaw import Mixer
from gpaw.poisson import PoissonSolver
# Dipole correction handled differently in svn/stable.
try:
    from gpaw.dipole_correction import (DipoleCorrectionPoissonSolver as DipoleCorrection)
except ImportError:
    from gpaw.dipole_correction import DipoleCorrection

def readimages(path):
    timages = []
    for adir in os.listdir(path):
        if os.path.isdir(os.path.join(path, adir)):
            for ftraj in os.listdir(os.path.join(path, adir)):
                if '.traj' in ftraj:
                    images = io.read(os.path.join(path, adir, ftraj),
                                    index=':')
                    timages.extend(images)
    return timages

atoms = io.read('Cu.traj')
atoms.set_pbc((0,1,0))
indices=[atom.index for atom in atoms if atom.tag==0]
atoms.set_constraint(FixAtoms(indices=indices))
selection = range(27,54)

ampcalc = Amp.load('./calc.amp')
qmcalc = GPAW(txt='qmcalc-out.txt',
              h=0.18,
              xc='RPBE',
              occupations=FermiDirac(0.1),
              eigensolver='rmm-diis',
              mixer=Mixer(beta=0.1, nmaxold=5, weight=50),
              spinpol=False,
              maxiter=400,
              poissonsolver=DipoleCorrection(PoissonSolver(), 2))
oldimages = readimages('/gpfs/data/ap31/coppertraj/gpaw_grid_traj')
convergence={'energy_maxresid': 5.0e-3,
             'force_rmse': 3.2e-2,
             'force_maxresid': 2.6e-1},

calc = AmpQMMM(selection=selection,
               qmcalc=qmcalc,
               mmcalc=ampcalc,
               qmvac=5,
               retrain=convergence,
               traintraj=oldimages)
atoms.set_calculator(calc)

dyn = BFGS(atoms, logfile='qn.log', trajectory='qn.traj' )
dyn.run(fmax=0.05)
print atoms.get_potential_energy()
